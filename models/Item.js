var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var itemSchema = new Schema({
    _id: Number,
    counts: [],
    createdAt: Date,
    key: String,
    value: String
});

module.exports = mongoose.model("records", itemSchema);
