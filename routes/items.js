var express = require('express');
var router = express.Router();

var Item = require("../models/Item");
var ISODate = require('isodate');

router.post("/", function (req, res, next) {
    Item.aggregate(
        [
            {$addFields :{ totalCount: { $sum: "$counts" } }},
            {$match: {
                    totalCount: { $gt: req.body.minCount , $lt: req.body.maxCount },
                    createdAt : { $gt: ISODate(req.body.startDate) , $lt: ISODate(req.body.endDate) }
                }}
        ]
    ).then((items) => {
        const newList = items.map(item => {
            var container = {};
            container.key = item.key;
            container.createdAt = item.createdAt;
            container.totalCount = item.totalCount;
            return container;
        });
        res.json({
            code: 0,
            msg: 'Success',
            records: newList
        });
    }).catch((err) => {
        res.json({
            code: -1,
            msg: err,
        });
    });

});


module.exports = router;
