const request = require('supertest');
const app = require('../app');

describe('Post Endpoints', () => {
    it('search items with given criteria', async () => {
        const res = await request(app)
            .post('/items')
            .send({
                startDate: '2016-01-26',
                endDate: '2018-02-02',
                minCount: 2700,
                maxCount: 3000
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('code', 0)
    })
});